const express = require("express");
const body_parser = require("body-parser");
const logger = require("morgan");
const methodOverride = require("method-override");
const app = express();

app.use(logger('dev'));
app.use(express.static(__dirname + "/bin/www/public"));
app.use(body_parser.urlencoded({extended: true}));
app.use(body_parser.json());
app.use(body_parser.json({type: "application/vnd.api+json"}));
app.use(methodOverride());

require("./server/routes")(app);
app.get('*', function (req, res) {
    //res.status(200).send('hello world');
    res.sendFile("index.html", {root: "./bin/www/public"});
});

module.exports = app;