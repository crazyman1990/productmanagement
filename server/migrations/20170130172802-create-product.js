'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('Products', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      prod_name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      quantity: {
        type: Sequelize.INTEGER,
        allowNull: false,
        defaultValue: 0
      },
      price: {
        type: Sequelize.FLOAT,
        allowNull: false,
        defaultValue: 0
      },
      prod_code: {
        type: Sequelize.STRING
      },
      img_thumb: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: "/img_thumbs/default_thumb.png"
      },
      img_path: {
        type: Sequelize.STRING,
        allowNull: true,
        defaultValue: "/imgs/default_img.png"
      },
      cateId: {
        type: Sequelize.INTEGER,
        references: {
          model: "Categories",
          key: 'id',
          as: 'cateId'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('Products');
  }
};