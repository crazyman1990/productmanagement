const Country = require("../models").Country;
const City = require("../models").City;

var Utils = {
    getAllCountries: function(req, res) {
        return Country.all().then(function(countries) {
            return res.status(200).json(countries);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    getCitiesByCountryId: function(req, res) {
        if (typeof req.params.id == 'undefined' || req.params.id == null) {
            return res.status(404).send({message: "Not found"});
        }
        return City.findAll({
            where: {
                countryId: req.params.id
            }
        }).then(function(cities) {
            return res.status(200).json(cities);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    }
};

module.exports = Utils;