const ProviderController = require("../controllers").Provider;

var providerRoute = function(app) {
    const providerUrl = "/api/providers";

    // GET all categories
    app.get(providerUrl, ProviderController.getAllProviders);

    // GET provider by id
    app.get(providerUrl + "/:id", ProviderController.getProviderById);

    // POST new provider
    app.post(providerUrl, ProviderController.createProvider);

    // PUT update provider
    app.put(providerUrl + "/:id", ProviderController.updateProvider);

    // DELETE provider
    app.delete(providerUrl + "/:id", ProviderController.deleteProvider);
};

module.exports = providerRoute;