const CustomerController = require("../controllers").Customer;

var customerRoute = function(app) {
    const customerUrl = "/api/customers";

    // GET all categories
    app.get(customerUrl, CustomerController.getAllCustomers);

    // GET customer by id
    app.get(customerUrl + "/:id", CustomerController.getCustomerById);

    // POST new customer
    app.post(customerUrl, CustomerController.createCustomer);

    // PUT update customer
    app.put(customerUrl + "/:id", CustomerController.updateCustomer);

    // DELETE customer
    app.delete(customerUrl + "/:id", CustomerController.deleteCustomer);
};

module.exports = customerRoute;