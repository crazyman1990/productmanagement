const AccountController = require("../controllers").Account;

var accountRoute = function(app) {

    const accountUrl = "/api/accounts";
    // GET all accounts
    app.get(accountUrl, AccountController.getAccounts);

    // GET account by id
    app.get(accountUrl + "/:id", AccountController.getAccountById);

    // POST new account
    app.post(accountUrl, AccountController.createAccount);

    // PUT update account
    app.put(accountUrl + "/:id", AccountController.updateAccount);

    // DELETE account
    app.delete(accountUrl + "/:id", AccountController.deleteAccount);
};

module.exports = accountRoute;