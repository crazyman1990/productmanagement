const Utilities = require("../helpers").Utilities;

var countryCityRoute = function(app) {

    const countryRoute = "/api/countries";
    const cityRoute = "/api/cities";
    app.get(countryRoute, Utilities.getAllCountries);

    app.get(cityRoute + "/:id", Utilities.getCitiesByCountryId);
};

module.exports = countryCityRoute;