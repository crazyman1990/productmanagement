const CategoryController = require("../controllers").Category;

var categoryRoute = function(app) {
    const categoryUrl = "/api/categories";

    // GET all categories
    app.get(categoryUrl, CategoryController.getAllCategories);

    // GET category by id
    app.get(categoryUrl + "/:id", CategoryController.getCategoryById);

    // POST new category
    app.post(categoryUrl, CategoryController.createCategory);

    // PUT update category
    app.put(categoryUrl + "/:id", CategoryController.updateCategory);

    // DELETE Category
    app.delete(categoryUrl + "/:id", CategoryController.deleteCategory);
};

module.exports = categoryRoute;