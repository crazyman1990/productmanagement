const ProductController = require("../controllers").Product;
const upload = require("../middlewares").upload;

var productRoute = function(app) {
    const productUrl = "/api/products";

    // GET all categories
    app.get(productUrl, ProductController.getAllProducts);

    // GET product by id
    app.get(productUrl + "/:id", ProductController.getProductById);

    // POST new product
    app.post(productUrl, upload("img_path"), ProductController.createProduct);

    // PUT update product
    app.put(productUrl + "/:id", upload("img_path"), ProductController.updateProduct);

    // DELETE product
    app.delete(productUrl + "/:id", ProductController.deleteProduct);
};

module.exports = productRoute;