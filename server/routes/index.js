const accountRoute = require("./accountRoute");
const categoryRoute = require("./categoryRoute");
const productRoute = require("./productRoute");
const customerRoute = require("./customerRoute");
const providerRoute = require("./providerRoute");
const countryCityRoute = require("./countryCityRoute");

var routes = function(app) {
    accountRoute(app);
    categoryRoute(app);
    productRoute(app);
    customerRoute(app);
    providerRoute(app);
    countryCityRoute(app);
};

module.exports = routes;