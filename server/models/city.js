'use strict';
module.exports = function(sequelize, DataTypes) {
  var City = sequelize.define('City', {
    city_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    city_code: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        City.belongsTo(models.Country, {
          foreignKey: "countryId",
          onDelete: "CASCADE"
        })
      }
    }
  });
  return City;
};