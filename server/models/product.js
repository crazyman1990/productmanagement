'use strict';
module.exports = function(sequelize, DataTypes) {
  var Product = sequelize.define('Product', {
    prod_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    quantity: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
      defaultValue: 0
    },
    prod_code: {
      type: DataTypes.STRING,
      allowNull: true
    },
    img_thumb: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: "/img_thumbs/default_thumb.png"
    },
    img_path: {
      type: DataTypes.STRING,
      allowNull: true,
      defaultValue: "/imgs/default_img.png"
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Product.belongsTo(models.Category, {
          foreignKey: "cateId",
          onDelete: "CASCADE"
        })
      }
    }
  });
  return Product;
};