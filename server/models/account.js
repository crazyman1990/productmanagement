'use strict';
const bcrypt = require("bcrypt");

module.exports = function(sequelize, DataTypes) {
  var Account = sequelize.define('Account', {
    username: {
      type: DataTypes.STRING,
      allowNull: false
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    salt: {
      type: DataTypes.STRING,
      allowNull: false
    },
    email: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Account.belongsTo(models.Role, {
          foreignKey: "roleId"
        });
      }
    },

    hooks: {
      beforeCreate: function(account, options, callback) {
        if (account.password) {
          hashPassword(account, options, callback);
        }
        return callback(null, options);
      },

      beforeUpdate: function(account, options) {
        if (account.password) {
          hashPassword(account, options, callback);
        }
        return callback(null, options);
      }
    },

    instanceMethods: {
      authenticate: function(value) {
        if (bcrypt.compareSync(value, this.password)) {
          return true;
        }
        return false;
      }
    }
  });
  return Account;
};

var hashPassword = function(account, options, callback) {
    if (!account.password) {
        throw new Error("Password cannot be empty");
    }
    bcrypt.genSalt(saltRound, function(err, salt) {
        if (err) {
            return callback(err);
        }
        bcrypt.hash(account.password, salt, function(err, hash) {
            if (err) {
                return callback(err);
            }
            account.password = hash;
            return callback(null, options);
        });
    });
};

