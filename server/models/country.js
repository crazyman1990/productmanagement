'use strict';
module.exports = function(sequelize, DataTypes) {
  var Country = sequelize.define('Country', {
    country_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    country_code: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Country.hasMany(models.City, {
          foreignKey: "countryId",
          as: "Cities"
        });
      }
    }
  });
  return Country;
};