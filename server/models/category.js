'use strict';
module.exports = function(sequelize, DataTypes) {
  var Category = sequelize.define('Category', {
    cate_name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Category.hasMany(models.Product, {
          foreignKey: "cateId",
          as: "products"
        })
      }
    }
  });
  return Category;
};