'use strict';
module.exports = function(sequelize, DataTypes) {
  var Provider = sequelize.define('Provider', {
    provider_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    }
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Provider.belongsTo(models.Country, {
          foreignKey: "countryId"
        });
        Provider.belongsTo(models.City, {
          foreignKey: "cityId"
        });
      }
    }
  });
  return Provider;
};