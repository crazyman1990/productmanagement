'use strict';
module.exports = function(sequelize, DataTypes) {
  var Customer = sequelize.define('Customer', {
    customer_name: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true
    },
    address: {
      type: DataTypes.STRING,
      allowNull: true
    }
    
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Customer.belongsTo(models.Country, {
          foreignKey: "countryId"
        });
        Customer.belongsTo(models.City, {
          foreignKey: "cityId"
        });
      }
    }
  });
  return Customer;
};