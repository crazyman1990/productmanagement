const multer = require("multer");
const storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "bin/www/public/imgs");
    },
    filename: function(req, file, callback) {
        if (file) {
            callback(null, file.originalname.replace(".", "-"  +  Date.now() + "."));
        } else {
            callback(null, "");
        }
    }
})
const uploader = multer({
    storage: storage
});

var uploadUtil = function(fieldname) {

    return function(req, res, next) {
        uploader.single(fieldname)(req, res, function() {
            if (req.file) {
                req.body[fieldname] = "/imgs/" + req.file.filename;
            }
            next();
        });
    };

};

module.exports = uploadUtil;