const Account = require("../models").Account;

var AccountController = {

    login: function(req, res) {
        return Account.findOne({
            where: {
                username: req.body.username
            }
        }).then(function(account) {
            if (!account || account.authenticate(req.body.password)) {
                return res.status(400).send({message: "Incorrect username or password"});
            }
            return res.status(200).send({message: "login successfull"});
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    createAccount: function(req, res) {
        return Account.create({
            username: req.body.username,
            password: req.body.password,
            email: req.body.email,
            roleId: req.body.roleId
        }).then(function (newAccount) {
            return res.status(201).json(newAccount);
        }).catch(function (err) {
            return res.status(500).send({message: err.message});
        });
    },

    getAccounts: function (req, res) {
        return Account.all().then(function(accounts) {
            return res.status(200).json(accounts);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    getAccountById: function (req, res) {
        return Account.findById(req.params.id).then(function(account) {
            if (!account) {
                return res.status(404).send({message: "Not found"});
            }
            return res.status(200).json(account);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    updateAccount: function(req, res) {
        return Account.findById(req.params.id).then(function(account) {
            if (!account) {
                return res.status(404).send({message: "Not found"});
            }
            return account.update({
                username: req.body.username,
                password: req.body.password,
                email: req.body.email
            }).then(function(result) {
                return res.status(200).json(result);
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    deleteAccount: function(req, res) {
        return Account.findById(req.params.id).then(function(account) {
            if (!account) {
                return res.status(404).send({message: "Not found"});
            }
            return account.destroy().then(function() {
                return res.status(200).send({message: "success"});
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    }
};

module.exports = AccountController;