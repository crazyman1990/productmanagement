const Product = require("../models").Product;

var ProductController = {

    getAllProducts: function(req, res) {
        return Product.all().then(function(products) {
            return res.status(200).json(products);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    getProductById: function(req, res) {
        return Product.findById(req.params.id).then(function(product) {
            if (!product) {
                return res.status(404).send({message: "Not found"});
            }
            return res.status(200).json(product);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    createProduct: function(req, res) {
        //console.log(req.body);
        return Product.create({
            prod_name: req.body.prod_name,
            quantity: req.body.quantity,
            price: req.body.price,
            cateId: req.body.cateId,
            prod_code: req.body.prod_code,
            img_path: req.body.img_path
        }).then(function(newProduct) {
            return res.status(201).json(newProduct);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    updateProduct: function(req, res) {
        return Product.findById(req.params.id).then(function(product) {
            if (!product) {
                return res.status(401).send({message: "Not found"});
            }
            return product.update({
                prod_name: req.body.prod_name,
                quantity: req.body.quantity,
                price: req.body.price,
                cateId: req.body.cateId,
                prod_code: req.body.prod_code,
                img_path: req.body.img_path
            }).then(function(result) {
                return res.status(200).json(result);
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    deleteProduct: function(req, res) {
        return Product.findById(req.params.id).then(function(product) {
            if (!product) {
                return res.status(404).send({message: "Not found"});
            }
            return product.destroy().then(function() {
                return res.status(200).send({message: "success"});
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    }

};

module.exports = ProductController;