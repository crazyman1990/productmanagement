const Customer = require("../models").Customer;

var CustomerController = {

    getAllCustomers: function(req, res) {
        return Customer.all().then(function(customers) {
            return res.status(200).json(customers);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    getCustomerById: function(req, res) {
        if (typeof req.params.id == 'undefined' || req.params.id == null) {
            return res.status(404).send({message: "Not found"});
        }
        return Customer.findById(req.params.id).then(function(customer) {
            if (!customer) {
                return res.status(404).send({message: "Not found"});
            }
            return res.status(200).json(customer);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    createCustomer: function(req, res) {
        return Customer.create({
            customer_name: req.body.customer_name,
            phone: req.body.phone,
            address: req.body.address,
            countryId: req.body.countryId,
            cityId: req.body.cityId
        }).then(function(newCustomer) {
            return res.status(201).json(newCustomer);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    updateCustomer: function(req, res) {
        return Customer.findById(req.params.id).then(function(customer) {
            if (!customer) {
                return res.status(401).send({message: "Not found"});
            }
            return Customer.update({
                customer_name: req.body.customer_name,
                phone: req.body.phone,
                address: req.body.address,
                countryId: req.body.countryId,
                cityId: req.body.cityId
            }).then(function(result) {
                return res.status(200).json(result);
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    deleteCustomer: function(req, res) {
        return Customer.findById(req.params.id).then(function(customer) {
            if (!customer) {
                return res.status(404).send({message: "Not found"});
            }
            return Customer.destroy().then(function() {
                return res.status(200).send({message: "success"});
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    }

};

module.exports = CustomerController;