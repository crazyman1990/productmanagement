const Provider = require("../models").Provider;

var ProviderController = {

    getAllProviders: function(req, res) {
        return Provider.all().then(function(providers) {
            return res.status(200).json(providers);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    getProviderById: function(req, res) {
        return Provider.findById(req.params.id).then(function(provider) {
            if (!provider) {
                return res.status(404).send({message: "Not found"});
            }
            return res.status(200).json(provider);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    createProvider: function(req, res) {
        return Provider.create({
            provider_name: req.body.provider_name,
            phone: req.body.phone,
            address: req.body.address,
            countryId: req.body.countryId,
            cityId: req.body.cityId
        }).then(function(newProvider) {
            return res.status(201).json(newProvider);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    updateProvider: function(req, res) {
        return Provider.findById(req.params.id).then(function(provider) {
            if (!provider) {
                return res.status(401).send({message: "Not found"});
            }
            return Provider.update({
                provider_name: req.body.provider_name,
                phone: req.body.phone,
                address: req.body.address,
                countryId: req.body.countryId,
                cityId: req.body.cityId
            }).then(function(result) {
                return res.status(200).json(result);
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    deleteProvider: function(req, res) {
        return Provider.findById(req.params.id).then(function(provider) {
            if (!provider) {
                return res.status(404).send({message: "Not found"});
            }
            return Provider.destroy().then(function() {
                return res.status(200).send({message: "success"});
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    }

};

module.exports = ProviderController;