const Account = require("./account");
const Category = require("./category");
const Product = require("./product");
const Provider = require("./provider");
const Customer = require("./customer");

module.exports = {
    Account, Category, Product, Provider, Customer
};