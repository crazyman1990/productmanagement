const Category = require("../models").Category;

var CategoryController = {

    getAllCategories: function(req, res) {
        return Category.all().then(function(categories) {
            return res.status(200).json(categories);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    getCategoryById: function(req, res) {
        return Category.findById(req.params.id).then(function(category) {
            if (!category) {
                return res.status(404).send({message: "Not found"});
            }
            return res.status(200).json(category);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    createCategory: function(req, res) {
        return Category.create({
            cate_name: req.body.cate_name
        }).then(function(newCategory) {
            return res.status(201).json(newCategory);
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    updateCategory: function(req, res) {
        return Category.findById(req.params.id).then(function(category) {
            if (!category) {
                return res.status(401).send({message: "Not found"});
            }
            return category.update({
                cate_name: req.body.cate_name
            }).then(function(result) {
                return res.status(200).json(result);
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    },

    deleteCategory: function(req, res) {
        return Category.findById(req.params.id).then(function(category) {
            if (!category) {
                return res.status(404).send({message: "Not found"});
            }
            return category.destroy().then(function() {
                return res.status(200).send({message: "success"});
            }).catch(function(err) {
                return res.status(500).send({message: err.message});
            });
        }).catch(function(err) {
            return res.status(500).send({message: err.message});
        });
    }

};

module.exports = CategoryController;