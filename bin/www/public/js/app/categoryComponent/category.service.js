(function() {
    var app = angular.module("productManagement");

    app.factory("categoryService", ['$http', '$q', function($http, $q) {
        var factory = {};
        const categoryUrl = "/api/categories";

        factory.getAllCategories = function() {
            return $http.get(categoryUrl).then(function(data) {
                //console.log(data.data);
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.createCategory = function(cate_name) {
            return $http.post(categoryUrl, {cate_name: cate_name}).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.editCategoryPopup = function(index, category) {
            
            return deferred.promise;
        };

        factory.editCategory = function(id, cate_name) {
            return $http.put(categoryUrl + "/" + id, {cate_name: cate_name}).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        return factory;
    }]);
})();