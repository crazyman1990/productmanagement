(function() {
    var app = angular.module("productManagement");

    app.component("listCategories", {
        bindings: {
            categories: "<"
        },
        templateUrl: "/js/app/categoryComponent/listCategories.template.html",
        controller: ["$q", "$state", "categoryService", 
        function($q, $state, categoryService) {
            var listCategories;
            this.$onInit = function() {
                listCategories = this.categories;
            }
            this.createCategory = function() {
                var deferred = $q.defer();
                swal({
                    title: 'New category',
                    input: 'text',
                    showCancelButton: true,
                    allowOutsideClick: false,
                    inputValidator: function (value) {
                        return new Promise(function (resolve, reject) {
                            if (value) {
                                categoryService.createCategory(value).then(function(category) {
                                    resolve(category);
                                }, function(err) {
                                    reject(err);
                                });
                            } else {
                                reject("Please enter category's name");
                            }
                        });
                    }
                }).then(function(result) {
                    deferred.resolve(result.data);
                });
                deferred.promise.then(function(category) {
                    listCategories.push(category);
                });
            };

            this.editCategory = function(index, category) {
                var deferred = $q.defer();
                swal({
                    title: 'Edit category',
                    input: 'text',
                    showCancelButton: true,
                    allowOutsideClick: false,
                    showLoaderOnConfirm: true,
                    inputValue: category.cate_name,
                    inputValidator: function (value) {
                        return $q(function (resolve, reject) {
                            if (value) {
                                //resolve();
                                categoryService.editCategory(category.id, value).then(function(category) {
                                    //category = cate;
                                    resolve(category);
                                }, function(err) {
                                    reject(err);
                                });
                            } else {
                                reject("Please enter category's name");
                            }
                        });
                    }
                }).then(function(result) {
                    deferred.resolve(result.data);
                });
                deferred.promise.then(function(category) {
                    listCategories[index] = category;
                });
            };

            this.deleteCategory = function(index) {
                this.categories.splice(index, 1);
            };
        }],
        controllerAs: "listCategoriesCtrl"
    });
})();