(function() {
    var app = angular.module("productManagement");

    app.component("menu", {
        templateUrl: '/js/app/menuComponent/menu.template.html',
        controller: ['$state', 
        function($state){
            var stateName = $state.current.name.toLowerCase();

            if (stateName.includes("product")) {
                this.currentSelected = 1;
            } else if (stateName.includes("categories")) {
                this.currentSelected = 2;
            } else if (stateName.includes("customer")) {
                this.currentSelected = 3;
            } else if (stateName.includes("provider")) {
                this.currentSelected = 4;
            }/* else if ($state.current.name.includes("products")) {
                this.currentSelected = 1;
            }*/
            //this.currentSelected = 1;

            this.setSelected = function(selected) {
                console.log('set');
                this.currentSelected = selected;
            };

            this.getSelected = function() {
                console.log('get');
                return this.currentSelected;
            };
        }],
        controllerAs: "menuCtrl"
    });
})();