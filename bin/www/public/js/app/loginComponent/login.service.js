(function() {
    var app = angular.module("productManagement");

    app.factory("loginService", ['$http', function($http) {

        var factory = {};
        const accountUrl = "/api/account";

        factory.login = function(account) {
            return $http.post(accountUrl + "/login", account).then(function(result) {
                return result;
            }, function(err) {
                return {status: err.status, message: err.data};
            });
        };

        factory.forgetPassword = function() {
            // TODO implementation
        };

        return factory;

    }]);
})();