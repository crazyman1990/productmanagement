(function() {
    var app = angular.module("productManagement");

    app.component("login", {
        templateUrl: "/js/app/loginComponent/login.template.html",
        controller: ['loginService', function(loginService) {
            this.account = {};

            this.login = function() {
                //loginService.login(this.account);
                console.log('login');
            };
        }],
        controllerAs: "loginCtrl"
    });
})();