(function() {
    var app = angular.module("productManagement");

    app.factory("UtilService", ['$http', '$q', 
    function($http, $q) {
        var factory = {};
        const countryUrl = "/api/countries";
        const cityUrl = "/api/cities";

        factory.getCities = function(id) {
            return $http.get(cityUrl + "/" + id).then(function(cities) {
                return cities.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.getCountries = function() {
            return $http.get(countryUrl).then(function(countries) {
                return countries.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        return factory;
    }]);
})();