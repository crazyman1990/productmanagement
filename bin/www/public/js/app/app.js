(function() {
    var app = angular.module('productManagement', 
        [
            'ui.router', 
            'ngMessages',
            'ngAnimate',
            'ngFileUpload',
            'ui.bootstrap',
            'angular-jwt',
            //'login'
        ]);
    app.constant("STATUS_CODE", {
        SUCCESS: 200,
        CREATED: 201,
        BAD_REQUEST: 400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        SERVER_ERROR: 500
    });

    app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', 
        function($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider){
            $urlRouterProvider.otherwise('/login');

            $stateProvider.state('login', {
                url: "/login",
                component: "login"
            }).state('homepage', {
                abstract: true,
                url: "",
                component: "homepage"
            }).state('homepage.products', {
                url: "/products",
                component: "listProducts",
                resolve: {
                    products: ["productService", 
                    function(productService) {
                        return productService.getAllProducts().then(function(products) {
                            return products;
                        });
                    }]
                }
            }).state('homepage.createProduct', {
                url: "/products/createProduct",
                component: "productDetail",
                resolve: {
                    product: function() {
                        return {};
                    },
                    categories: ["categoryService", function(categoryService) {
                        //return [{id: 1, cate_name: 'cate1'}, {id: 2, cate_name:'cate2'}];
                        return categoryService.getAllCategories().then(function(categories) {
                            return categories;
                        });
                    }]
                }
            }).state('homepage.editProduct', {
                url: "/products/{id:[1-9]+}",
                component: "productDetail",
                resolve: {
                    product: ["$stateParams", "productService", 
                    function($stateParams, productService) {
                        return productService.getProductById($stateParams.id).then(function(product) {
                            return product;
                        });
                    }],
                    categories: ["categoryService", function(categoryService) {
                        return categoryService.getAllCategories().then(function(categories) {
                            return categories;
                        });
                    }]
                }
            }).state('homepage.categories', {
                url: "/categories",
                component: "listCategories",
                resolve: {
                    categories: ["categoryService", 
                    function(categoryService) {
                        return categoryService.getAllCategories().then(function(categories) {
                            return categories;
                        });
                    }]
                }
            }).state('homepage.customers', {
                url: "/customers",
                component: "listCustomers",
                resolve: {
                    customers: ["customerService", function(customerService) {
                        return customerService.getAllCustomers().then(function(customers) {
                            return customers;
                        });
                    }]
                }
            }).state('homepage.createCustomer', {
                url: "/customers/createCustomer",
                component: "customerDetail",
                resolve: {
                    customer: function() {
                        return {};
                    },
                    countries: ["UtilService", function(UtilService) {
                        return UtilService.getCountries().then(function(countries) {
                            return countries;
                        });
                    }]
                }
            }).state('homepage.editCustomer', {
                url: "/customers/{id:[1-9]+}",
                component: "customerDetail",
                resolve: {
                    customer: ["$stateParams", "customerService", 
                    function($stateParams, customerService) {
                        return customerService.getCustomerById($stateParams.id).then(function(customer) {
                            return customer;
                        });
                    }],
                    /*cities: ["UtilService", function(UtilService) {
                        //return [{id: 1, cate_name: 'cate1'}, {id: 2, cate_name:'cate2'}];
                        return UtilService.getCities().then(function(cities) {
                            return cities;
                        });
                    }],*/
                    countries: ["UtilService", function(UtilService) {
                        return UtilService.getCountries().then(function(countries) {
                            return countries;
                        });
                    }]
                }
            }).state('homepage.providers', {
                url: "/providers",
                component: "listProviders",
                resolve: {
                    providers: ["providerService", function(providerService) {
                        return providerService.getAllProviders().then(function(providers) {
                            return providers;
                        });
                    }]
                }
            }).state('homepage.createProvider', {
                url: "/providers/createProvider",
                component: "providerDetail",
                resolve: {
                    provider: function() {
                        return {};
                    },
                    countries: ["UtilService", function(UtilService) {
                        return UtilService.getCountries().then(function(countries) {
                            return countries;
                        });
                    }]
                }
            }).state('homepage.editProvider', {
                url: "/providers/{id:[1-9]+}",
                component: "providerDetail",
                resolve: {
                    provider: ["$stateParams", "providerService", 
                    function($stateParams, providerService) {
                        return providerService.getProviderById($stateParams.id).then(function(provider) {
                            return provider;
                        });
                    }],
                    /*cities: ["UtilService", function(UtilService) {
                        //return [{id: 1, cate_name: 'cate1'}, {id: 2, cate_name:'cate2'}];
                        return UtilService.getCities().then(function(cities) {
                            return cities;
                        });
                    }],*/
                    countries: ["UtilService", function(UtilService) {
                        return UtilService.getCountries().then(function(countries) {
                            return countries;
                        });
                    }]
                }
            });

            $locationProvider.html5Mode(true);
        }])
        .run(['$rootScope', '$state', '$stateParams', 'jwtHelper', 'STATUS_CODE', 
        function($rootScope, $state, $stateParams, jwtHelper, STATUS_CODE){
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }]);
})();