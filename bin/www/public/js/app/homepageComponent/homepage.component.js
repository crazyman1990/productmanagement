(function() {
    var app = angular.module("productManagement");

    app.component('homepage', {
        templateUrl: '/js/app/homepageComponent/homepage.template.html',
        controller: [function() {

        }],
        controllerAs: "homepageCtrl"
    });
})();