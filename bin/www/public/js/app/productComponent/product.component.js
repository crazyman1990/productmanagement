(function() {
    var app = angular.module("productManagement");

    app.component("listProducts", {
        bindings: {
            products: '<'
        },
        templateUrl: "/js/app/productComponent/listProducts.template.html",
        controller: ["productService", function(productService) {
            
        }],
        controllerAs: "listProductCtrl"
    });
})();