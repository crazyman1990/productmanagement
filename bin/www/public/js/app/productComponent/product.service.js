(function() {
    var app = angular.module("productManagement");

    app.factory("productService", ['$http', '$q', "Upload", 
    function($http, $q, Upload) {
        var factory = {};
        const productUrl = "/api/products";

        factory.getAllProducts = function() {
            return $http.get(productUrl).then(function(data) {
                //console.log(data.data);
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.getProductById = function(id) {
            return $http.get(productUrl + "/" + id).then(function(product) {
                return product.data;
            }, function(err) {
                return $q.reject(err);
            });
        },

        factory.saveProduct = function(product) {
            return Upload.upload({
                method: product.id === "undefined" || product.id == null ? "POST" : "PUT",
                url: productUrl,
                data: product
            }).then(function(data) {
                console.log(data.data);
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        return factory;
    }]);
})();