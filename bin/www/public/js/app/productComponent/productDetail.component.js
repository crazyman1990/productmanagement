(function() {
    var app = angular.module("productManagement");

    app.component("productDetail", {
        bindings: {
            product: "<",
            categories: "<"
        },
        templateUrl: "/js/app/productComponent/productDetail.template.html",
        controller: ["$state","productService", 
        function($state, productService) {
            this.$onInit = function() {
                if (!this.product.img_path) {
                    console.log('in');
                    this.product.img_path = '/imgs/default_img.png';
                }
            };

            this.setSelectedCategory = function(category) {
                this.product.cateId = category.id;
                this.product.category = category;
            };

            this.saveProduct = function() {
                this.product.cateId = this.category.id;
                console.log(this.product);
                productService.saveProduct(this.product).then(function(result) {
                    $state.go("homepage.products");
                });
            };

            this.changeFile = function(file) {
                if (!file) {
                    this.product.img_path = '/imgs/default_img.png';
                }
            };
        }],
        controllerAs: "productCtrl"
    });
})();