(function() {
    var app = angular.module("productManagement");

    app.factory("customerService", ['$http', '$q', 
    function($http, $q) {
        var factory = {};
        const customerUrl = "/api/customers";

        factory.getAllCustomers = function() {
            return $http.get(customerUrl).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.getCustomerById = function(id) {
            return $http.get(customerUrl + "/" + id).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.addCustomer = function(customer) {
            return $http.post(customerUrl, customer).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.saveCustomer = function(id, customer) {
            return $http.put(customerUrl + "/" + id, customer).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        return factory;
    }]);
})();