(function() {
    var app = angular.module("productManagement");

    app.component("customerDetail", {
        templateUrl: "/js/app/customerComponent/customerDetail.template.html",
        bindings: {
            customer: "<",
            countries: "<"
        },
        controller: ["customerService", "UtilService", 
        function(customerService, UtilService) {
            var ctrl = this;
            ctrl.getCities = function(item, model, label, event) {
                UtilService.getCities(item.id).then(function(cities) {
                    ctrl.cities = cities;
                });
            };
            
            ctrl.saveCustomer = function() {
                if (ctrl.customer.id) {
                    customerService.saveCustomer($stateParams.id, ctrl.customer).then(function(data) {

                    });
                } else {
                    customerService.addCustomer(ctrl.customer).then(function(data) {

                    });
                }
            };
        }],
        controllerAs: "customerCtrl"
    });
})();