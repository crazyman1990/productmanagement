(function() {
    var app = angular.module("productManagement");

    app.component("listCustomers", {
        templateUrl: "/js/app/customerComponent/listCustomers.template.html",
        bindings: {
            customers: "<"
        },
        controller: [function() {
            
        }],
        controllerAs: "listCustomersCtrl"
    });
})();