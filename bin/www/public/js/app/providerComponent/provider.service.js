(function() {
    var app = angular.module("productManagement");

    app.factory("providerService", ['$http', '$q', 
    function($http, $q) {
        var factory = {};
        const providerUrl = "/api/providers";

        factory.getAllProviders = function() {
            return $http.get(providerUrl).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.getProviderById = function(id) {
            return $http.get(providerUrl + "/" + id).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.addProvider = function(provider) {
            return $http.post(providerUrl, provider).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        factory.saveProvider = function(id, provider) {
            return $http.put(providerUrl + "/" + id, provider).then(function(data) {
                return data.data;
            }, function(err) {
                return $q.reject(err);
            });
        };

        return factory;
    }]);
})();