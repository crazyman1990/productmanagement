(function() {
    var app = angular.module("productManagement");

    app.component("providerDetail", {
        templateUrl: "/js/app/providerComponent/providerDetail.template.html",
        bindings: {
            provider: "<"
        },
        controller: ["providerService", function(providerService) {
            this.saveProvider = function() {
                if (this.provider.id) {
                    providerService.saveProvider($stateParams.id, this.provider).then(function(data) {

                    });
                } else {
                    providerService.addProvider(this.provider).then(function(data) {

                    });
                }
            };
        }],
        controllerAs: "providerCtrl"
    });
})();