(function() {
    var app = angular.module("productManagement");

    app.component("listProviders", {
        templateUrl: "/js/app/providerComponent/listProviders.template.html",
        bindings: {
            providers: "<"
        },
        controller: [function() {
            
        }],
        controllerAs: "listProvidersCtrl"
    });
})();